---
layout: handbook-page-toc
title: "Referrals"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Referrals

If you know someone who would be a great addition to the GitLab team, please submit them as a Referral. If they're successfully hired and all Referral criteria are met, you'll receive a Referral Bonus!

While the Referral program is fairly straight-forward, there are several nuances to be mindful of. Please take a moment to review the program rules and eligibility outlined below.

#### Defining a Referral

While "sourcing" and "referring" are similar activities, there are distinctions based on their respective familiarity and confidence in regards to the prospective candidate.

A Referral is:

* Someone you are claiming a personal- or professional relationship with.
* Someone you could make a confident claim about regarding their cultural adaptability (to GitLab), how well they align with our values, and their ability to excel in a given role.
* Someone you feel would be a great addition to the GitLab team.

What a Referral is **not**:

* Someone you **do not** know and **would not** be able to speak about confidently.
* Someone you sourced or someone who reached out to you regarding a vacancy (e.g. on LinkedIn)
* Someone who applied via a link you shared on a social site.
    * There's **no** way of determining who clicked a link on Twitter from a genuine Referral.

#### Referral Bonus Eligibility

All GitLab team members are eligible for the [Referral Bonus](https://about.gitlab.com/handbook/incentives/#referral-bonuses), except in the following circumstance:

* If the Referring Team Member is a part of the **Hiring Team** for the referred vacancy.
    * e.g. a *Recruiter* or *Hiring Manager*.
        * If the situation allows, a Hiring Team Member (**excluding** the *Recruiter* and *Hiring Manager*) can recuse themselves from interviewing the Referral.
* If the Referring Team Member would manage the Referral directly.
* If there's a perceived conflict of interest.

#### Submitting a Referral

All GitLab Team Members can [submit a referral](https://support.greenhouse.io/hc/en-us/articles/201982560-Submit-Referrals-from-the-Dashboard) directly from their [Greenhouse dashboard](https://app2.greenhouse.io/dashboard). Here's how to do that:

1.  Click the **"+"** button, then **"Add a Referral."**
    * To note, you **don't** need to select an "Office," as this will **limit** the vacancies you're able to select.
2. Complete the required fields on the **Referral In-Take Form**.
    * Please provide context as to why you're referring the candidate (i.e. why would they be a great addition to the GitLab team?).
3. If you **don't** have their resume, please include a link to their LinkedIn profile.

#### Referral Statuses

You can stay up-to-date on the status of your referral(s) in the **My Referrals** section of your Greenhouse dashboard. If you have any questions regarding your referral, please sent an email to the referrals@domain. Please note that the Recruiting Team is **unable** to provide candidate-specific feedback to Referrers (i.e you); only to the Candidate themselves. You can expect Referrals to be reviewed by the Hiring Team within **10-days** of their submission.

Although we place special emphasis on Referrals, we treat Referral interviews the same as we do non-Referrals. Referral interviews should not be more or less demanding than any other interview.

To prevent wrongful hires, Referrers need to keep their bar high. Interviewers and Hiring Managers should avoid bias based on the Referrer's status. There should be **no** favoritism based on previous experience working with a Referral after the candidate is hired.

#### Common Referral Situations

* Your Referral already submitted an application.
    * In the event that your Referral already applied, please send an email to the referrals@domain. In that email, please  answer all the questions outlined in the **Referral In-Take Form**. To reiterate, Referrals should be submitted with a personal conviction that they'll be a great addition to the GitLab team. After receiving that, the appropriate Team Member will look into updating the Candidate's source information.
* Your Referral was referred by someone else.
    * In the event that your Referral was submitted by someone else, the first Team Member to have submitted the candidate will be considered their Referrer.
        * In the event of multiple Referrers claiming ownership on a similar vacancy, it will be up to the Referrers to work out how to split the Referral Bonus and inform PeopleOps of the compromise.
* If you referred the candidate to dissimilar vacancy, then you would be named their Referrer for the specific vacancy.
    

Other scenarios to be mindful of:

* If the Referrer is part of the Hiring Team...
    * ... they'll recuse themselves from interviewing the candidate and the Hiring Manager will choose a replacement Interviewer.
* When a Referral is from a Referring Manager...
    * ... they may choose to move the candidate on in the interview process, even if the candidate received a "no" from a fellow Interviewer - we **don't** operate on a [single-veto basis](#single-vetos).
    * ... will review all information and feedback about the Referral (giving both positive and negative feedback fair consideration) and will make the final hire or no hire decision.
    
For information about how referral bonuses are processed please reference [this page](https://about.gitlab.com/handbook/incentives/#referral-bonuses).

For any additional questions regarding how to make a referral, please email referrals@domain.
